package com.zuitt.example;

public class Dog extends Animal {
    private String breed;

    public Dog(String breed){
        super("name", "color");
        this.breed = breed;
    }

    public String getBreed(){
        return this.breed;
    }

    /*methods*/
    public void speak(){
        super.call(); /*The call() method from Animal Class*/
        System.out.println("Woof Woof!");
    }
}
