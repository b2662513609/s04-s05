package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car();
        System.out.println("This car is driven by " + myCar.getDriverName());

        Dog myDog = new Dog("Chow-chow");
        myDog.setName("Beary");
        myDog.setColor("Brown");

        myDog.speak();

        System.out.println(myDog.getName() + " " + myDog.getBreed() + " " + myDog.getColor());

        //Abstraction
        /*is a process where all the logic and complexity are hidden from the user.*/
        Person child = new Person();

        child.sleep();

        //Polymorphism
        /*Delivered from the greek word: poly means "many" and morph means "forms."*/
        StaticPoly myAddition = new StaticPoly();
        System.out.println(myAddition.addition(5,6));
        System.out.println(myAddition.addition(5,6, 10));
        System.out.println(myAddition.addition(5.5,6.6));
    }
}
