import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Contact person1 = new Contact("Rafhael", "09672358541", "Caloocan City");
        Contact person2 = new Contact();
        person2.setName("Arbis");
        person2.setContactNumber("09672358541");
        person2.setAddress("Caloocan City");

        Phonebook phonebook = new Phonebook();

        phonebook.setContacts(person1);
        phonebook.setContacts(person2);

        ArrayList<Contact> contacts = phonebook.getContacts();
        if (contacts.size() > 0) {
            for (Contact contact : contacts) {
                System.out.println("Name: " + contact.getName());
                System.out.println("Contact Number: " + contact.getContactNumber());
                System.out.println("Address: " + contact.getAddress());
                System.out.println();
            }
        } else {
            System.out.println("Phonebook is empty.");
        }
    }
}