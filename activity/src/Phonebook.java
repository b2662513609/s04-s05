import java.util.ArrayList;

public class Phonebook {
    ArrayList<Contact> contacts = new ArrayList();

    public Phonebook() {
        this.contacts = new ArrayList<Contact>();
    }
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }

    public void setContacts(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }
}
